#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_TinyUSB.h>
#include <MIDI.h>

/// amount of ws2812b leds
#define STRIP_LENGTH 8*2*4
/// note count (button cound)
#define NOTES STRIP_LENGTH/4
/// strip brightness
#define DEFAULT_BRIGHTNESS 30

/// WS2812B data pin
#define PIN_DT 13
// mux channels
#define PIN_S0 21
#define PIN_S1 20
#define PIN_S2 19
#define PIN_S3 18
/// mux analog read channel
#define PIN_SIG 28

// pressure steps
#define PRESSURE_LOWEST (initial_values[i] * 2/3) // w initial~600 -> 400
#define PRESSURE_LOW (PRESSURE_LOWEST * 2/3) // about 300
#define PRESSURE_HIGH (PRESSURE_LOW * 2/3)  // about 200
#define PRESSURE_HIGHEST (PRESSURE_HIGH * 2/3) // about 150

Adafruit_NeoPixel pixels(STRIP_LENGTH, PIN_DT, NEO_GRB + NEO_KHZ800);
Adafruit_USBD_MIDI usb_midi;

// create midi instance
MIDI_CREATE_INSTANCE(Adafruit_USBD_MIDI, usb_midi, MIDI);

/// last read value of pressure sensor
float last_values[NOTES];
float initial_values[NOTES];
/// current octave
/// always refers to C
int octave = 60;

// control button state
float max_pressure[4];

// led colors
int THEME_IDLE[3] = {0x3c, 0x38, 0x36}; // fg1
int THEME_OFF[3] = {0x00, 0x00, 0x00}; // black

int THEME_FADE_1[3] = {0x8f, 0x3f, 0x71}; // purple
int THEME_FADE_2[3] = {0x79, 0x74, 0x0e}; // green
int THEME_FADE_3[3] = {0xaf, 0x3a, 0x03}; // orange
int THEME_FADE_4[3] = {0x9d, 0x00, 0x06}; // red

int THEME_NATURAL[3] = {0xb5, 0x76, 0x14}; // yellow
int THEME_FLAT[3] = {0x07, 0x66, 0x78}; // blue

void setup() {
#if defined(ARDUINO_ARCH_MBED) && defined(ARDUINO_ARCH_RP2040)
  // Manual begin() is required on core without built-in support for TinyUSB such as mbed rp2040
  TinyUSB_Device_Init(0);
#endif

  TinyUSBDevice.setManufacturerDescriptor("comcloudway@polarplayer-studio  ");
  TinyUSBDevice.setProductDescriptor     ("Floe                            ");
  usb_midi.setStringDescriptor           ("Floe                            ");

  MIDI.begin(MIDI_CHANNEL_OMNI);

  Serial.begin(115200);

  // wait until device mounted
  while( !TinyUSBDevice.mounted() ) delay(1);

  pinMode(PIN_S0, OUTPUT);
  pinMode(PIN_S1, OUTPUT);
  pinMode(PIN_S2, OUTPUT);
  pinMode(PIN_S3, OUTPUT);
  pinMode(PIN_SIG, INPUT);

  for (int i = 0; i<NOTES; i++) {
    digitalWrite(PIN_S0, (i >> 0) & 1);
    digitalWrite(PIN_S1, (i >> 1) & 1);
    digitalWrite(PIN_S2, (i >> 2) & 1);
    digitalWrite(PIN_S3, (i >> 3) & 1);

    float v = analogRead(PIN_SIG);
    last_values[i]=v;
    initial_values[i] = v;
  }
  for (int c = 0; c<4; c++) {
    int i = c + 12;
    max_pressure[c]=PRESSURE_LOWEST;
  }

  pixels.begin();
  pixels.setBrightness(DEFAULT_BRIGHTNESS);
  for (int i = 0; i<STRIP_LENGTH; i++) {
    pixels.setPixelColor(i, pixels.Color(
                           THEME_IDLE[0],
                           THEME_IDLE[1],
                           THEME_IDLE[2]
));
  }
  pixels.show();
}

void loop() {
  for (int i = 0; i<NOTES; i++) {
    digitalWrite(PIN_S0, (i >> 0) & 1);
    digitalWrite(PIN_S1, (i >> 1) & 1);
    digitalWrite(PIN_S2, (i >> 2) & 1);
    digitalWrite(PIN_S3, (i >> 3) & 1);

    float v = analogRead(PIN_SIG);

    Serial.println(v);
    float last = last_values[i];

    int leds[4];
    if (i==3) {
      leds[0]=54;
      leds[1]=55;
      leds[2]=56;
      leds[3]=57;
    } else if (i==2) {
      leds[0]=38;
      leds[1]=39;
      leds[2]=40;
      leds[3]=41;
    } else if (i==1) {
      leds[0]=22;
      leds[1]=23;
      leds[2]=24;
      leds[3]=25;
    } else if (i==0) {
      leds[0]=6;
      leds[1]=7;
      leds[2]=8;
      leds[3]=9;
    } else if (i==7) {
      leds[0]=4;
      leds[1]=5;
      leds[2]=10;
      leds[3]=11;
    } else if (i==6) {
      leds[0]=20;
      leds[1]=21;
      leds[2]=26;
      leds[3]=27;
    } else if (i==5) {
      leds[0]=36;
      leds[1]=37;
      leds[2]=42;
      leds[3]=43;
    } else if (i==4) {
      leds[0]=52;
      leds[1]=53;
      leds[2]=58;
      leds[3]=59;
    } else if (i==11) {
      leds[0]=50;
      leds[1]=51;
      leds[2]=60;
      leds[3]=61;
    } else if (i==10) {
      leds[0]=34;
      leds[1]=35;
      leds[2]=44;
      leds[3]=45;
    } else if (i==9) {
      leds[0]=18;
      leds[1]=19;
      leds[2]=28;
      leds[3]=29;
    } else if (i==8) {
      leds[0]=2;
      leds[1]=3;
      leds[2]=12;
      leds[3]=13;
    } else if (i==15) {
      leds[0]=0;
      leds[1]=1;
      leds[2]=14;
      leds[3]=15;
    } else if (i==14) {
      leds[0]=16;
      leds[1]=17;
      leds[2]=30;
      leds[3]=31;
    } else if (i==13) {
      leds[0]=32;
      leds[1]=33;
      leds[2]=46;
      leds[3]=47;
    } else if (i==12) {
      leds[0]=48;
      leds[1]=49;
      leds[2]=62;
      leds[3]=63;
    }
float pressure_perc = 0.0;

    if (v <= PRESSURE_LOWEST && v>=PRESSURE_HIGHEST) {
      pressure_perc = (1.0 -
                       (v - PRESSURE_HIGHEST) /
                       (PRESSURE_LOWEST - PRESSURE_HIGHEST));
    } else if (v < PRESSURE_HIGHEST) {
      // send max pressure
      pressure_perc = 1.0;
    } else if (v >= PRESSURE_LOWEST) {
      // send pressure 0
      pressure_perc = 0.0;
    }

    if (v<PRESSURE_HIGHEST) {
      for (int led = 0; led<4; led++) {
        pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FADE_4[0] * pressure_perc + THEME_FADE_3[0] * (1 - pressure_perc),
            THEME_FADE_4[1] * pressure_perc + THEME_FADE_3[1] * (1 - pressure_perc),
            THEME_FADE_4[2] * pressure_perc + THEME_FADE_3[2] * (1 - pressure_perc)
));
      }
      pixels.show();

    } else if (v<PRESSURE_HIGH) {
      for (int led = 0; led<4; led++) {
        pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FADE_3[0] * pressure_perc + THEME_FADE_2[0] * (1 - pressure_perc),
            THEME_FADE_3[1] * pressure_perc + THEME_FADE_2[1] * (1 - pressure_perc),
            THEME_FADE_3[2] * pressure_perc + THEME_FADE_2[2] * (1 - pressure_perc)
));
      }
      pixels.show();

    } else if (v < PRESSURE_LOW) {
      for (int led = 0; led<4; led++) {
        pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FADE_2[0] * pressure_perc + THEME_FADE_1[0] * (1 - pressure_perc),
            THEME_FADE_2[1] * pressure_perc + THEME_FADE_1[1] * (1 - pressure_perc),
            THEME_FADE_2[2] * pressure_perc + THEME_FADE_1[2] * (1 - pressure_perc)
));
      }
      pixels.show();

    } else if (v<PRESSURE_LOWEST) {
      for (int led = 0; led<4; led++) {
        if (i < 12) {
      int base = (i + octave) % 12;
      if (base == 1 || base == 3 || base == 6 || base == 8 || base == 10) {
 pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FADE_1[0] * pressure_perc + THEME_FLAT[0] * (1-pressure_perc),
            THEME_FADE_1[1] * pressure_perc + THEME_FLAT[1] * (1-pressure_perc),
            THEME_FADE_1[2] * pressure_perc + THEME_FLAT[2] * (1-pressure_perc)
));
      } else {
         pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FADE_1[0] * pressure_perc + THEME_NATURAL[0] * (1-pressure_perc),
            THEME_FADE_1[1] * pressure_perc + THEME_NATURAL[1] * (1-pressure_perc),
            THEME_FADE_1[2] * pressure_perc + THEME_NATURAL[2] * (1-pressure_perc)
));
      }
        } else {
        pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FADE_1[0] * pressure_perc + THEME_IDLE[0] * (1-pressure_perc),
            THEME_FADE_1[1] * pressure_perc + THEME_IDLE[1] * (1-pressure_perc),
            THEME_FADE_1[2] * pressure_perc + THEME_IDLE[2] * (1-pressure_perc)
));
        }

      }
      pixels.show();
    } else if (i < 12) {
      int base = (i + octave) % 12;
      for (int led = 0; led<4; led++) {
      if (base == 1 || base == 3 || base == 6 || base == 8 || base == 10) {
  pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_FLAT[0],
            THEME_FLAT[1],
            THEME_FLAT[2]
));

      } else {
         pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_NATURAL[0],
            THEME_NATURAL[1],
            THEME_NATURAL[2]
));

      }
            }
      pixels.show();
    } else {
      for (int led = 0; led < 4; led++) {
   pixels.setPixelColor(
          leds[led],
          pixels.Color(
            THEME_IDLE[0],
            THEME_IDLE[1],
            THEME_IDLE[2]
));
        }
    };
    last_values[i]=v;

    if (i < 12) {

    if (last > PRESSURE_LOWEST && v <= PRESSURE_LOWEST) {
      // on
      MIDI.sendNoteOn(octave+i, 127, i%16+1);
    } else if (v > PRESSURE_LOWEST && last <=PRESSURE_LOWEST) {
      // off
      MIDI.sendNoteOff(octave+i, 0, i%16+1);
    }

    MIDI.sendAfterTouch(127*pressure_perc, i%16+1);
  }
    else if (i==14) {
      for (int channel = 0; channel < NOTES; channel++) {
        MIDI.sendPitchBend((1-2*pressure_perc)*0.5, channel);
      }
      }
    else {

    if (v < PRESSURE_LOWEST && v < max_pressure[i-12]) {
      max_pressure[i - 12] = v;
    } else if (v >= PRESSURE_LOWEST && last <PRESSURE_LOWEST) {
      if (i == 12) {
        if (max_pressure[i - 12] < PRESSURE_HIGH) {
          // fast mode
          if (octave>=12) {
            octave-=12;
          }
        } else {
          // slow mode
          if (octave >= 1) {
            octave -= 1;
          }
        }
      } else if (i==15) {
        if (max_pressure[i-12] < PRESSURE_HIGH) {
          // fast mode
          if (octave+12<=127) {
            octave+=12;
          }
        } else {
          // slow mode
          if (octave+1 <= 127) {
            octave += 1;
          }
        }
      } else if (i == 13) {
        octave = 60;
      }

      max_pressure[i] = PRESSURE_LOWEST;
    }

    }
  }

  delay(50);
}
