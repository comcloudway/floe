This file and the README.org file are licensed under [[http://creativecommons.org/licenses/by-sa/4.0/][CC bY-SA 4.0]],
the Creative Commons Attribution ShareAlike 4.0 License.

The icon.png has been copied from the [[https://codeberg.org/comcloudway/floe/src/branch/gimp][gimp]] branch
and is thus licensed under the license specified by the given [[https://codeberg.org/comcloudway/floe/src/branch/gimp/LICENSE.org][license]].
